package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
)

func main() {

	DEFAULTDIR := "."
	DEFAULTPORT := "27107"

	flag.Parse()

	if len(flag.Args()) < 1 {
		fmt.Println("example: go-ftp-simple-server . 27107")
		os.Exit(0)
	}

	DEFAULTDIR = flag.Arg(0)
	DEFAULTPORT = flag.Arg(1)

	fmt.Println("DIR:", DEFAULTDIR, "is sharing. Now Listening port:", DEFAULTPORT)

	h := http.FileServer(http.Dir(DEFAULTDIR))
	http.ListenAndServe(":"+DEFAULTPORT, h)
}
